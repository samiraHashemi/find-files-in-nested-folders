import os
from docx import Document

def find_folder_with_files(folder_path):
    doc = Document()
    allowed_extensions = ['txt', 'yml', 'py', 'yaml', 'css', 'html'] #This is the list for allowed extensions
    for root, dirs, files in os.walk(folder_path):
        for file in files:
            for ext in allowed_extensions:
                if file.lower().endswith(ext):
                    with open(os.path.join(root, file), 'r') as f:
                        file_content = f.read()
                        doc.add_heading(root + " - " + os.path.basename(file), level=1)
                        doc.add_paragraph(file_content)
                        doc.add_page_break()
                    doc.save("finalOutPut.docx") #The document name for last results
    return None

folder_path = './one' #In this line to need to give the folder path './one' is the sample folder that we have it
find_folder_with_files(folder_path)